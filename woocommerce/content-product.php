<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>

    <div class="xl-product">
        <div class="xl-product-inner">

            <div class="xl-product-image-wrap">
                <?php woocommerce_show_product_loop_sale_flash(); ?>
                <div class="xl-product-image">
                    <a href="<?php the_permalink();?>"><?php woocommerce_template_loop_product_thumbnail(); ?></a>
                </div><!--/.xl-product-image-->
            </div><!--/.xl-product-image-wrap-->

            <div class="xl-product-content">
                <div class="xl-product-content-inner">
                    <div class="xl-product-title">
                        <a href="<?php the_permalink();?>"><?php woocommerce_template_loop_product_title(); ?></a>
                    </div><!--/.xl-product-title-->
                    <div class="content"><?php echo get_the_excerpt(); ?></div>
                    <div class="xl-product-price">
                        <?php woocommerce_template_loop_price(); ?>
                    </div><!--/.xl-product-price-->
                    <div class="d-flex align-items-center justify-content-between xl-product-ratting-price">
                        <div class="ratting"><?php woocommerce_template_loop_rating(); ?></div>
                        <div class="cart"><?php woocommerce_template_loop_add_to_cart(); ?></div>
                    </div><!--/.xl-product-ratting-->
                </div><!--/.xl-product-content-inner-->
            </div><!--/.xl-product-content-->
        </div><!--/.xl-product-inner-->
    </div><!--/.xl-product-->
</li>
